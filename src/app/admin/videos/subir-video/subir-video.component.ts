import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, FormGroupDirective, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AdminService } from 'src/app/services/admin.service';
import { ToastrService } from 'ngx-toastr';
import { Course, Video } from 'src/app/interfaces/interfaces';
import { switchMap } from 'rxjs';
import { FileUpload } from 'src/app/admin/models/file-upload-model';

@Component({
  selector: 'app-subir-video',
  templateUrl: './subir-video.component.html',
  styleUrls: ['./subir-video.component.css']
})
export class SubirVideoComponent implements OnInit {

  @ViewChild(FormGroupDirective) formulario!: FormGroupDirective;
  @ViewChild('txtBuscar') txtBuscar!: ElementRef<HTMLInputElement>;

  video: Video ={
    id: '',
    title: '',
    course: {
      id :'',
      name :'',
      description :'',
      views : 0
    },
    publication_date: '',
    url:'',
    likes: 0
  };

  materias!: Course[];
  currentFileUpload?: FileUpload;
  materiasAux!: any[];
  imagenSeleccionada = false;
  selectedFiles?: any;
  percentage = 0;
  tipo: string = 'agregar';
  format: string = '';
  url: any;
  visible = true;

  loading: boolean = true;
  disabled: boolean = false;
  scrollable: boolean = true;

  youtubeLinkRegExp = /^.*(youtu.be\/|v\/|u\/\w\/|embed\/|watch\?v=|\&v=|\?v=)([^#\&\?]*).*/;

  miFormulario: FormGroup = this.fb.group({
    title: [ '', [ Validators.required, Validators.minLength(3) ] ],
    url: [ '', [ Validators.required, Validators.minLength(3), Validators.pattern(this.youtubeLinkRegExp) ] ],
    course: [ , [ Validators.required ] ],
    keywords: [ '', [ Validators.required ] ],
    file: [ '' ],
  })

  constructor( private fb: FormBuilder,
    private adminService: AdminService,
    private toastr: ToastrService,
    private activatedRoute: ActivatedRoute,
    private router: Router

) { }

  campoNoValido( campo: string) {
    return this.miFormulario.get(campo)?.invalid && this.miFormulario.get(campo)?.touched;
  }

  compareObjects(o1: any, o2: any): boolean {
    return o1?.name === o2?.name && o1?.id === o2?.id;
  }

  selectFile(event: any): void {
    const file = event.target.files && event.target.files[0];

    if (file) {
      // Obtener el nombre del archivo directamente del objeto file
      const filename: string = file.name;
      console.log('Nombre del archivo:', filename);

      //Previsualización de la imagen
      if (file.type.indexOf('image') > -1) {
        this.selectedFiles = event.target.files;
        this.format = 'image';
        const reader = new FileReader();
        reader.onload = (event) => {
          this.url = (<FileReader>event.target).result;
        };
        reader.readAsDataURL(file);
      } else {
        this.selectedFiles = null;
        this.url = null;
        this.toastr.error('Por favor, solo subir archivos de formato imagen', 'Error');
      }
    } else {
      this.url = null;
      this.selectedFiles = null;
    }
  }


  ngOnInit(): void {

    this.adminService.obtenerMateriasVideos().subscribe( materias => {
      this.materias = materias;
      this.materiasAux = materias;
    });

    if( !this.router.url.includes('editar') ) {
      return;
    }

    // Si la ruta es de editar traer la informacion segun su id para rellenar los campos del formulario
    this.activatedRoute.params
      .pipe(
        switchMap( ({id}) => this.adminService.obtenerVideoPorId(id) )
      )
      .subscribe( (video: Video) => {
        //Rellenar el formulario con la informacion obtenida
        this.video = video;
        this.miFormulario.reset({
          ...this.video
        });
      });

  }



  agregarVideo() {
    if( this.miFormulario.invalid ){
      this.miFormulario.markAllAsTouched();
      return;
    }

    if( this.video.id ){

      // Actualizar Video
      this.tipo = 'editar';
      this.video = {
        ...this.video,
        ...this.miFormulario.value,
      }
      if (this.selectedFiles) {

        this.adminService.eliminarVideoStorage(this.video.photo_filename!)

        let filename: string = this.miFormulario.controls['file'].value;
        filename = filename.split('\\').slice(-1)[0];
        this.video.photo_filename = filename;
        delete this.video.file;

        const file: File | null = this.selectedFiles.item(0);
        this.selectedFiles = undefined;

        if (file) {
          this.disabled = true;
          this.disableForm();
          this.currentFileUpload = new FileUpload(file);
          this.adminService.agregarVideo(this.currentFileUpload, this.video, this.tipo).subscribe( percentage => {
            this.percentage = Math.round(percentage ? percentage : 0);
            if( this.percentage == 100 ){
              setTimeout(() => {
                this.toastr.info('El video fue actualizado con éxito', 'Video Actualizado');
                this.enableForm();
                this.url = null;
                this.visible = false;
                this.percentage = 0;
                this.disabled = false;
              }, 500);
            }
          });
        } else {
          this.toastr.error('Por favor, seleccione una imagen para subir', 'Error')
        }

      } else {

        const { photo_filename, file, ...videoData } = this.video;

        this.adminService.actualizarVideo(videoData)
          .then( ( _ ) => {
            this.toastr.info('El video fue actualizado con éxito', 'Video Actualizado')
          })
          .catch( err => {
            console.log(err);
            this.toastr.error(`err`, 'Error');
          })
      }

    } else {
      // Crear Video
      this.video = {
        ...this.miFormulario.value,
        publication_date: new Date()
      }
      if (this.selectedFiles) {
        let filename: string = this.miFormulario.controls['file'].value;
        filename = filename.split('\\').slice(-1)[0];
        this.video.photo_filename = filename;
        delete this.video.file;

        const file: File | null = this.selectedFiles.item(0);
        this.selectedFiles = undefined;
        if (file) {
          this.disabled = true;
          this.disableForm();
          this.currentFileUpload = new FileUpload(file);
          this.visible = true;
          this.adminService.agregarVideo(this.currentFileUpload, this.video, this.tipo).subscribe( percentage => {
            this.percentage = Math.round(percentage ? percentage : 0);
            if( this.percentage == 100 ){
              setTimeout(() => {
                this.toastr.success(`El video ${this.video.title} fue subido con éxito!`, 'Video Subido');
                this.enableForm();
                this.miFormulario.reset();
                this.url = null;
                this.visible = false;
                this.percentage = 0;
                this.disabled = false
              }, 500);
            }
          });
        } else {
          this.toastr.error('Error al subir el video', 'Error')
        }
      } else {
        this.toastr.error('Por favor, seleccione una imagen para subir', 'Error')
      }
    }

  }


  filtrarMaterias($event: any) {
    let valor: string = $event.target.value.toLowerCase().trim();

    this.materias = this.materiasAux
    this.materias = this.materias.filter((materia: Course) => {
      const nombreMateria = materia.name.toLowerCase();
      if( nombreMateria.includes(valor) ) {
        return true;
      } else {
        return false;
      }
    });
  }

  limpiarBuscador() {
    this.txtBuscar.nativeElement.value = '';
    this.materias = this.materiasAux;
  }
  enableForm(): void {
    this.miFormulario.controls['title'].enable();
    this.miFormulario.controls['course'].enable();
    this.miFormulario.controls['url'].enable();
    this.miFormulario.controls['keywords'].enable();
    this.miFormulario.controls['file'].enable();
  }

  disableForm(): void {
    this.miFormulario.controls['title'].disable();
    this.miFormulario.controls['course'].disable();
    this.miFormulario.controls['url'].disable();
    this.miFormulario.controls['keywords'].disable();
    this.miFormulario.controls['file'].disable();
  }
}
