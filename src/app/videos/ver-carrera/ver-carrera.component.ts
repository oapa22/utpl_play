import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AdminService } from 'src/app/services/admin.service';
import { switchMap } from 'rxjs';
import { Career, Course } from 'src/app/interfaces/interfaces';

@Component({
  selector: 'app-ver-carrera',
  templateUrl: './ver-carrera.component.html',
  styleUrls: ['./ver-carrera.component.css']
})
export class VerCarreraComponent implements OnInit {

  carrera!: Career;
  materias: Course[] = [];
  preMaterias: Course[] = [];
  carrerasRelacionadas!: Career[];

  constructor( private activatedRoute: ActivatedRoute,
               private adminService: AdminService
  ) { }

  ngOnInit(): void {

    this.adminService.obtenerMaterias().subscribe( materias => {
      this.preMaterias = materias;
    });

    this.activatedRoute.params
      .pipe(
        switchMap( ({id}) => this.adminService.obtenerCarreraPorId(id) )
      )
      .subscribe( carrera => {
        this.carrera = carrera;
        this.materias = this.preMaterias.filter(preMat => preMat.careers?.some(c => c.id === carrera.id));
        this.adminService.obtenerCarrerasPorArea(this.carrera.area).subscribe( carreras => {
          this.carrerasRelacionadas = carreras.filter( carrera => carrera.id != this.carrera.id )
        })
      });

  }
}
