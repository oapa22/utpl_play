import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { switchMap, tap } from 'rxjs';
import { AdminService } from 'src/app/services/admin.service';
import { AuthService } from 'src/app/services/auth.service';
import { Records, User, Video } from 'src/app/interfaces/interfaces';

@Component({
  selector: 'app-ver-video',
  templateUrl: './ver-video.component.html',
  styleUrls: ['./ver-video.component.css']
})
export class VerVideoComponent implements OnInit {

  user!: User;
  video!: Video;
  videoHistorial: boolean = false;
  videoView: boolean = false;
  videos!: Video[];
  record!: Records;
  liked: boolean = false;

  constructor( private adminService: AdminService,
               private activatedRoute: ActivatedRoute,
               private authService: AuthService,
               private router: Router ) {

  }

  ngOnInit() {

    this.authService.obtenerClaims()
    .pipe(
      switchMap( idTokenResult => this.adminService.obtenerUsuarioPorId(idTokenResult?.claims['user_id']) )
    )
    .subscribe( user => {
      this.user = user;
    });

    this.activatedRoute.params
      .pipe(
        switchMap( ({id}) => this.adminService.obtenerVideoPorId(id) ),
        tap( video => {
          this.video = video;
          this.record = {
            id: this.video.id,
            name: this.video.title
          }
        }),
        switchMap( video => this.adminService.obtenerVideosPorMateria(video.course) )
      )
      .subscribe(videos => {
          this.videos = videos;
          this.videos = this.videos.filter( video => video.id != this.video.id );
          this.updateLikedStatus();
        }
      )
  }

  cambiarVideo( id: string ){
    this.router.navigate(['/play/video', id]);
    this.videoHistorial = false;
    this.videoView = false;
    this.updateLikedStatus();
  }

  toggleLike() {
    if (this.liked) {
      // Quitar like
      this.video.likes = (this.video.likes ?? 0) - 1;
      this.user.likedVideos = this.user.likedVideos?.filter(
        (videoId) => videoId !== this.video.id
      );
    } else {
      // Dar like
      this.video.likes = (this.video.likes ?? 0) + 1;
      if (!this.user.likedVideos) {
        this.user.likedVideos = [this.video.id];
      } else {
        this.user.likedVideos.push(this.video.id);
      }
    }
    this.liked = !this.liked;
    this.adminService.actualizarVideo(this.video);
    this.authService.actualizarUsuario(this.user);

  }

  updateLikedStatus() {
    this.liked = this.isLiked() || false;
  }

  isLiked() {
    return this.user.likedVideos?.includes(this.video.id);
  }

  historial(event: any){

  }

}
