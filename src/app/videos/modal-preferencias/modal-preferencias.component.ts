import { Component, OnInit } from '@angular/core';
import { SwitchService } from 'src/app/services/switch.service';
import { AdminService } from 'src/app/services/admin.service';
import { Career, User } from 'src/app/interfaces/interfaces';
import { AuthService } from 'src/app/services/auth.service';
import { take } from 'rxjs/operators';
@Component({
  selector: 'app-modal-preferencias',
  templateUrl: './modal-preferencias.component.html',
  styleUrls: ['./modal-preferencias.component.css']
})
export class ModalPreferenciasComponent implements OnInit {

  carreras: Career[] = [];
  usuario!: User;
  preferencias: string[] = [];
  selectedPreferences: string[] = [];
  guardandoPreferencias = false; // Variable para controlar el estado de guardado

  constructor(
    private authService: AuthService,
    private modalss: SwitchService,
    private adminService: AdminService
  ) {}

  ngOnInit(): void {
    this.authService.obtenerUsuarioActual().subscribe((usuario) => {
      if (usuario && usuario.career) {
        const carrera = usuario.career;

        this.preferencias = this.obtenerPreferencias(carrera);
        this.obtenerPreferenciasSeleccionadas(usuario.uid);
      }
    });

    this.adminService.obtenerCarreras().subscribe((carreras) => {
      this.carreras = carreras;
    });
  }

  obtenerPreferencias(carrera: string): string[] {
    const carreraSeleccionada = this.carreras.find((c) => c.name === carrera);
    if (carreraSeleccionada && carreraSeleccionada.keywords) {
      return carreraSeleccionada.keywords;
    }
    return [];
  }

  obtenerPreferenciasSeleccionadas(uid: string) {
    this.adminService.obtenerUsuarioPorId(uid).subscribe((usuario: User) => {
      if (usuario && usuario.preferences) {
        this.selectedPreferences = usuario.preferences;
      }
    });
  }

  closeModal() {
    this.modalss.$modal.emit(false);
  }

  togglePreference(preferencia: string) {
    const index = this.selectedPreferences.indexOf(preferencia);
    if (index !== -1) {
      // La preferencia ya estaba seleccionada, la eliminamos
      this.selectedPreferences.splice(index, 1);
    } else {
      // La preferencia no estaba seleccionada, la agregamos
      this.selectedPreferences.push(preferencia);
    }
    console.log('Selected Preferences:', this.selectedPreferences);
  }

  isSelected(preferencia: string): boolean {
    const isSelected = this.selectedPreferences.includes(preferencia);
    return isSelected;
  }

  guardarPreferencias() {
    if (this.guardandoPreferencias) {
      // Si ya se está guardando, no hacer nada
      return;
    }

    // Establecer la bandera de guardado en true
    this.guardandoPreferencias = true;

    // Guardar las preferencias en el usuario
    this.authService.obtenerUsuarioActual().pipe(take(1)).subscribe((usuario) => {
      if (usuario) {
        usuario.preferences = this.selectedPreferences;
        this.authService.actualizarUsuario(usuario).then(() => {
          console.log('Preferencias guardadas correctamente');
          // Cerrar el modal o realizar otras acciones necesarias
          this.closeModal
          // Restablecer la bandera de guardado en false después de completar la operación
          this.guardandoPreferencias = false;
        }).catch((error) => {
          console.error('Error al guardar las preferencias:', error);

          // Restablecer la bandera de guardado en false en caso de error
          this.guardandoPreferencias = false;
        });
      }
    });
  }

}
